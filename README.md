# StandardIssue Pelican Theme

This is basically StandardIssue template (https://templated.co/standardissue) turned into a Pelican theme. Obviously, the template was never meant to work with Pelican specifically, so some imagination had to be involved in adapting it to Pelican needs, although the goal was to make as few alterations as possible.

## Screenshot

![Sample screenshot](StandardIssue-Pelican-small.png)

[Also available as full page view](StandardIssue-Pelican.png)

## The theme introduces some extra variables:

Too enable the respective functionality, add the required variables to pelicanconf.py file:

Add a "version" or similar superscript to the title

`SITESUPERSCRIPT = ''`

Enable google search for the site

`GOOGLE_SEARCH = True|False`

Show the "about the author" box

`ABOUT_THE_AUTHOR = True|False`

The contents of the textbox with author info are set in the "textbox_author.html" file

Show all articles in the sidebar, no grouping

`SIDEBAR_SHOW_ALL_ARTICLES_BY_DATE = True|False`

The list can be limited to n first entries by setting

`MAX_ARTICLES_BY_DATE = n`

If the number of articles exceeds n, an ellipsis is added, leading to the archives.html page.

Show all articles in the  sidebar, by category

`SIDEBAR_SHOW_ALL_ARTICLES_BY_CATEGORY = True|False`

The list can be limited to n first entries by setting

`MAX_ARTICLES_BY_CATEGORY = n`

If the number of articles exceeds n, an ellipsis is added, leading to the index page of the category in question.

Show all pages in the sidebar, no grouping

`SIDEBAR_SHOW_ALL_PAGES = True|False`

Currently, if DISQUS_SITENAME is set, the comment forms by default appear only for articles.
Show comment form (currently only Disqus is available) on pages as well:

`ENABLE_COMMENTS_FOR_PAGES = True|False`

Show some text in the footer (where the blogroll links and suchlike are normally found):

`FOOTER_TEXTBOX = True|False`

The contents of the textbox are set in the "textbox_footer.html" file

## Git* ribbons:

### Github (from notmyidea theme):

`GITHUB_URL = ''`

`GITHUB_POSITION = 'left'|'right'`

### Gitlab:

`GITLAB_URL = ''`

`GITLAB_POSITION = 'left'|'right'`

## Horizontal menu position

The current CSS version assumes the line will be long, and places it lower. If you won't have a lot there, and you want to have the menu exactly where it is in the original StandardIssue template, change in the CSS file the definition of #menu from

```css
#menu {
	float: right;
	clear: both;
	margin-top: 10px;
	margin-bottom: 15px;
}
```

to

```css
#menu {
	float: right;
}
```

## TODO

- [X] Add "Fork me on Gitlab" ribbon (mod of https://github.com/blog/273-github-ribbons)
- [X] Enable Disqus integration
	- [ ] Figure out whether inconsistent comment count is my fault or server issues
- [ ] Enable social media share buttons

## Licensing

Since the original template was released under Creative Commons Attribution license, so is this theme.

![CC BY 3.0](cc-by-88x31.png)